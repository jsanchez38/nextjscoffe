import { GetServerSidePropsContext, GetServerSidePropsResult, NextPage } from 'next';
import { NextSeo } from 'next-seo';
import Link from 'next/link';
import React, { useState } from 'react';
import styles from './cofeel.module.scss';

interface ParsedUrlQuery {
    id: string;
  }

const coffes : NextPage  = (props:any) => {

  console.log(props)
  const {id,posts} = props;
  const isEmpty = Object.keys(posts).length === 0;

  return (
    <>
      <NextSeo title={"test"} />
      <h1>test</h1>
      <Link href="/coffe">
          <a>Coffes</a>
        </Link>
      {!isEmpty ?(
        <div>
         <div  key={posts.id}  className={styles.cardDetalles}>

          <div className={styles.container}>
          <p>ID: {posts.id}</p>
           <p>NAME: {posts.name}</p>
           <p>BRAND: {posts.brand}</p> 
           <p> RECOMENDATION :{posts.recommendation}</p>

           <div>
             {posts.flavors.length> 0 ? (posts.flavors.map((flavorsitem:any)=><p key={flavorsitem.id}>flavors : {flavorsitem.name}</p>)):("no tiene")}
           </div>
        </div>
        </div>
        </div> 
        
          
      
        
       
  ):("no hay valores")}
    </>
  );
};

export async function getServerSideProps(
    Props: GetServerSidePropsContext,
  ): Promise<GetServerSidePropsResult<any>> {
    const { locale, params } = Props;
    console.log(Props)
    const { id } = params as unknown as ParsedUrlQuery;
    const res = await fetch(`http://localhost:3010/coffees/${id}`);
    const posts = await res.json();
    console.log(posts)
    return {
      props: {
        id:id,
        posts
      },
    };
  }
// }
// export async function getServerSideProps(
//   Props: GetServerSidePropsContext,
// ): Promise<GetServerSidePropsResult<any>> {
//   const { locale } = Props;
//   const fetchapi =async () => {
//     const response = await fetch("http://localhost:3010/coffees");
//     const data = await response.json()
//     console.log(data)

  

//   }
//   return {  
//     props: {
//       data
//       //fetchapi:fetchapi,

//     },
//   };


export default coffes;
